import { Alert } from 'bootstrap'
import React, { useState } from 'react'
import { Link } from 'react-router-dom'
import Footer from '../components/Footer'
import Navbar from '../components/Navbar'

export default function Signup() {
    const [credentials,setcredentials]=useState({name:"",email:"",password:""})
    const handleSubmit=async(e)=>{
        e.preventDefault()
        const response = await fetch("http://localhost:5000/api/createuser",{
            method:'POST',
            headers:{
                'Content-Type':'application/json'
            },
            body:JSON.stringify({name:credentials.name,email:credentials.email,password:credentials.password})
        })
        const json= await response.json()
        console.log(json);
        if(!json.success){
            alert("Enter Valid Credentials")
        }
    }
    const onChange=(event)=>{
        setcredentials({...credentials,[event.target.name]:event.target.value})
    }
    const handleChange=()=>{
        alert('Created Account && you can login on clicking "Already a user"')
    }
    return (
        <>
        <Navbar/>
        <div className='container' style={{textAlign:'center'}}>
        <h1>Welcome to 'Foodies'!</h1>
        </div>
        <div className='container' style={{marginTop:'50px',border:'3px solid #D1D1D1',padding:'20px',backgroundColor:'#D1D1D1',borderRadius:'20px'}}>
            <form onSubmit={handleSubmit}>
            <div className="mb-3">
                    <label htmlFor="name" className="form-label">
                        Name
                    </label>
                    <input
                        type="text"
                        className="form-control"
                        name='name'
                        value={credentials.name}
                        onChange={onChange}
                    />
                    </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputEmail1" className="form-label" >
                        Email address
                    </label>
                    <input
                        type="email"
                        className="form-control"
                        name='email'
                        value={credentials.email}
                        onChange={onChange}
                        id="exampleInputEmail1"
                        aria-describedby="emailHelp"
                    />
                    <div id="emailHelp" className="form-text">
                        We'll never share your email with anyone else.
                    </div>
                </div>
                <div className="mb-3">
                    <label htmlFor="exampleInputPassword1" className="form-label">
                        Password
                    </label>
                    <input
                        type="password"
                        className="form-control"
                        name='password'
                        value={credentials.password}
                        onChange={onChange}
                        id="exampleInputPassword1"
                    />
                </div>
               
                <button type="submit" className="m-3 btn btn-success" onClick={handleChange}>
                    Submit
                </button>
                <Link to='/login' className='m-3 btn btn-danger'>Already a user</Link>
            </form>
            
            </div>
            <div style={{position:'absolute',bottom:'0',width:'100%'}}>
      <Footer/>
      </div>
            

        </>
    )
}
