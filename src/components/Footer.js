import React from 'react';
import { Link } from 'react-router-dom';

export default function Footer() {
  return (
    <div id='foot' >
     <footer className="text-center bg-body-tertiary" style={{ backgroundColor: "#304146",color:'white' }}>
  <div className="container pt-4">
    <img  src='https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTqsmWvYQRcg2PelJeUwQP6pIQwXer4tz7AM8Tp_SEtgQ&s' style={{height:"30px",width:"30px",margin:"10px"}}/>
    <img src='https://upload.wikimedia.org/wikipedia/commons/thumb/9/95/Instagram_logo_2022.svg/1200px-Instagram_logo_2022.svg.png' style={{height:"30px",width:"30px",margin:"10px"}}/>
    <img src='https://cdn-icons-png.flaticon.com/512/124/124021.png' style={{height:"30px",width:"30px",margin:"10px"}}/>
    <img src='https://play-lh.googleusercontent.com/kMofEFLjobZy_bCuaiDogzBcUT-dz3BBbOrIEjJ-hqOabjK8ieuevGe6wlTD15QzOqw' style={{height:"30px",width:"30px",margin:"10px"}}/>
    <img src='https://upload.wikimedia.org/wikipedia/commons/e/ef/Youtube_logo.png?20220706172052' style={{height:"30px",width:"30px"}}/>

    
  </div>
  <div className='text-center p-2'>
    <p>You can talk with us on <Link to='/'>Talent.com</Link></p>
  </div>

  <div
    className="text-center p-2"
  >
    © 2024 Copyright  Foodies
  </div>
</footer>

    </div>
  );
}
