import React, { useState } from 'react'
import { Link,useNavigate } from 'react-router-dom'
import  Badge  from 'react-bootstrap/Badge';
import Modal from '../Modal';
import Cart from '../screens/Cart';
import { useCart } from './ContextReducer';

export default function Navbar() {
  const [cartview,setCartView]=useState(false)
let data=useCart();
const navigate=useNavigate();
const handleLogout=()=>{
localStorage.removeItem("authToken");
navigate("/")
}


  return (
    <div>
      <nav className="navbar navbar-expand-lg navbar-dark bg-dark" style={{position:'fixed',width:'100%',zIndex:'9999',top:'0'}}>
        <Link className="navbar-brand foodies fs-1"  to="/">
          Foodies
        </Link>
        <button
          className="navbar-toggler"
          type="button"
          data-toggle="collapse"
          data-target="#navbarNav"
          aria-controls="navbarNav"
          aria-expanded="false"
          aria-label="Toggle navigation"
        >
          <span className="navbar-toggler-icon" />
        </button>
        <div className="collapse navbar-collapse" id="navbarNav">
          <ul className="navbar-nav me-auto mb-2">
            <li className="nav-item "> 
              <Link className="nav-link active fs-6" aria-current="page" to="/">
                Home
              </Link>
            </li>
              <li className="nav-item ">
                <a className="nav-link active fs-6" aria-current="page" href='#foot'>
                Contact us
                </a>
              </li>
              
          </ul>
          {(!localStorage.getItem("authToken")) ?
            <div className='d-flex mx-3'>
              <Link className="btn bg-white text-success mx-3" to="/login">
                Login
              </Link>
              <Link className="btn bg-white text-success mx-3" to="/createuser">
                Signup
              </Link>
            </div>
            :
            <div>
            <div className='btn bg-white text-success mx-2' onClick={()=>{setCartView(true)}}>
              My Cart {" "}
              <Badge pill bg="danger"> {data.length === 0 ? '' : data.length > 1 ? data.length : data.length} </Badge>
            </div> 
            {cartview? <Modal onClose={()=>setCartView(false)} ><Cart /></Modal>:null}
            <div className='btn bg-white text-danger mx-2' onClick={handleLogout}>
              Logout
            </div> 
            </div>
            }
        </div>
      </nav>

    </div>
  )
}
