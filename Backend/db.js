const mongoose = require('mongoose')
const mongoURI='mongodb://firstmodule:foodies@ac-pyjouoo-shard-00-00.ma2eknm.mongodb.net:27017,ac-pyjouoo-shard-00-01.ma2eknm.mongodb.net:27017,ac-pyjouoo-shard-00-02.ma2eknm.mongodb.net:27017/foodies?ssl=true&replicaSet=atlas-nfdecm-shard-0&authSource=admin&retryWrites=true&w=majority'
const mongoDB=async()=>{
 await mongoose.connect(mongoURI,{useNewUrlParser: true,useUnifiedTopology: true},async(err,res)=>{
    if(err) console.log(err);
    else{
        console.log("connected");
        const fetched_data = await mongoose.connection.db.collection("fooditems")
        fetched_data.find({}).toArray(async function(err,data){
            const foodcategory = await mongoose.connection.db.collection("foodcategory");
            foodcategory.find({}).toArray(function(err,catData){
                if(err) console.log(err);
            else {
                global.fooditems=data;
                global.foodcategory=catData;
            }
            })
        })
    }
});
}
module.exports=mongoDB;